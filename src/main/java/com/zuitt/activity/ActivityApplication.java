package com.zuitt.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpHeaders;

@SpringBootApplication
@RestController
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	@PostMapping("/users")
	public String postUser(){
		return "New user created.";
	}

	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved.";
	}

	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "User with user ID# " + userid + " retrieved.";
	}

	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid, @RequestHeader(value = "Authorization") String authorization){
		String message = "";
		if(!authorization.isEmpty())
			message = "The user " + userid + " has been deleted.";
		else
			message = "Unauthorized access.";

		return message;
	}

	@PutMapping("/users/{userid}")
	public User putUser (@PathVariable Long userid, @RequestBody User user){
		return user;
	}
}
